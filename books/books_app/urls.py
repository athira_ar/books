from django.urls import path

from . import views
from django.conf.urls import url

urlpatterns = [
    path('', views.index, name='index'),
    path('book_names', views.book_names, name='book_names'),
    path('book_details', views.book_details, name='book_details'),
    path('user_login', views.user_login, name='user_login'),
    path('registration', views.registration, name='registration'),
    path('register_function', views.register_function, name='register_function'),

]
