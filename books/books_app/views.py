from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import render
from django.db import connections
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

from .backend.config_data import config_json
from django.views.decorators.csrf import csrf_exempt
import json
# Create your views here.

def index(request):
    # return HttpResponse("Hello, world. You're at the polls index.")
    print('here index')
    title = "MyClub Event Calendar "
    cal = "ATHIRA"
    filen = "books_app/index.html"
    # return render(request, 'books_app/base.html', {'title': title, 'cal': cal, 'filen': filen})
    return render(request, 'books_app/index.html')
@csrf_exempt
def user_login(request):
    print('here login')
    if request.method == 'POST':
        username = request.POST.get('uname')
        password = request.POST.get('password')
        if (username=="admin" and password=="admin@123"):
            return HttpResponse(json.dumps({'res': "success"}),
                                content_type="application/json")
        else:
            return HttpResponse(json.dumps({'res': "error"}),
                                content_type="application/json")

@csrf_exempt
def registration(request):
    print('here registration')

    filen = "books_app/registration.html"
    title = "ADD NEW BOOKS"
    return render(request, 'books_app/base.html',
                  {'training_status': 'active', 'config_json': config_json, 'title': title, 'filen': filen})

    return render(request, 'books_app/registration.html')
@csrf_exempt
def register_function(request):
    print('here registration1')
    cursor = connections['default'].cursor()


    book_name = request.POST.get('book_name')
    author = request.POST.get('author')
    book_count = request.POST.get('book_count')

    cursor = connections['default'].cursor()
    print(cursor)
    sql = "insert into  book_details (book_name, author, books_count) values(%s, %s, %s) "
    cursor.execute(sql, (book_name, author, book_count))
    connections['default'].commit()

    val = "Successfully added book details "
    return HttpResponse(json.dumps({'res': val}), content_type="application/json")
@csrf_exempt
def book_names(request):
    # print('inside squad')
    cursor = connections['default'].cursor()
    cursor.execute("select id,book_name, author, books_count from book_details")
    fieldnames = [name[0] for name in cursor.description]
    result = []
    for row in cursor.fetchall():
        row_set = []
        for field in zip(fieldnames, row):
            row_set.append(field)
        result.append(dict(row_set))
    connections['default'].close()
    config_json.update({"teamdata": result})

    filen = "books_app/book_names.html"
    title = "BOOK LIST"
    return render(request, 'books_app/base.html',  {'home_status': 'active','config_json':config_json,'title': title,'filen': filen})

@csrf_exempt
def book_details(request):
    print('inside details')
    id = request.POST.get('id')
    cursor = connections['default'].cursor()
    # sql = "select id,book_name, author, books_count from book_details WHERE id ="+id
    cursor.execute("select id,book_name, author, books_count from book_details")
    fieldnames = [name[0] for name in cursor.description]
    result = []
    for row in cursor.fetchall():
        row_set = []
        for field in zip(fieldnames, row):
            row_set.append(field)
        result.append(dict(row_set))
    connections['default'].close()
    config_json.update({"teamdata": result})

    filen = "books_app/book_details.html"
    title = "BOOK DETAILS"
    return render(request, 'books_app/base.html',  {'home_status': 'active','config_json':config_json,'title': title,'filen': filen})


