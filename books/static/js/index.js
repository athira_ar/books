$(document).ready(function() {
    $("#user_login").submit(function(){
        var frm = $('#user_login');
        $.ajax({
            type:"POST",
            url:"user_login",
            data:frm.serialize(),
            success: function (data) {
                var json = jQuery.parseJSON(JSON.stringify(data));
                console.log(json)
                if (json.res == 'success') {
                    window.location = "book_names";
                } else {
                    alert('incorrect username or password');
                }
            }
        });
    });
});